import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Label label = new Label();

        Button btnStart = new Button();
        btnStart.setText("Start");
        btnStart.setOnAction(event -> {
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    for (int i = 0; i < 10000; i++) {
                        if (isCancelled()) break;
                        int finalI = i;
                        Platform.runLater(() -> label.setText(Integer.toString(finalI)));
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException interrupted) {
                            interrupted.printStackTrace();
                        }
                    }
                    return null;
                }
            };
            new Thread(task).start();
        });

        VBox root = new VBox();
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        root.getChildren().add(label);
        root.getChildren().add(btnStart);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}




